package org.spring.springboot.dao.master;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.spring.springboot.domain.WebUser;

import java.util.List;
import java.util.Map;

/**
 * 用户 DAO 接口类
 *
 * Created by bysocket on 07/02/2017.
 */
@Mapper
public interface WebUserMDao {

    List<WebUser> findAll();

    void addDate(WebUser webUser);

    List<WebUser> findByDate(@Param("params") Map<String, Object> param);

    List<String> getAllMobile();

}
