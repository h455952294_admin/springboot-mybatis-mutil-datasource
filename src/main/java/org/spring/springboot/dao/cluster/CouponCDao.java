package org.spring.springboot.dao.cluster;

import org.apache.ibatis.annotations.Mapper;
import org.spring.springboot.domain.CouponC;

/**
 * 城市 DAO 接口类
 *
 * Created by bysocket on 07/02/2017.
 */
@Mapper
public interface CouponCDao {

    CouponC readCoupon(String couponId);
}
