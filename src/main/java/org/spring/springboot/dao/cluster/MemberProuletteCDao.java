package org.spring.springboot.dao.cluster;

import org.apache.ibatis.annotations.Mapper;
import org.spring.springboot.domain.MemberProulette;

import java.util.List;

/**
 */
@Mapper
public interface MemberProuletteCDao {

    void batchSave(List<MemberProulette> list);
}
