package org.spring.springboot.dao.cluster;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.spring.springboot.domain.WebUserC;

import java.util.List;
import java.util.Map;

/**
 * 城市 DAO 接口类
 *
 * Created by bysocket on 07/02/2017.
 */
@Mapper
public interface WebUserCDao {

    /*List<WebUser> findAll();

    void addDate(WebUser webUser);*/

    void updateDate(WebUserC webUserC);

    void insertDate(WebUserC webUserC);

    WebUserC findByMId(@Param("phone") String phone);

    void insertMemberAuthority(@Param("params") Map<String, Object> map);

    List<Map<String,Object>> findMemberAuthority(@Param("phone") String phone);

    void batchSave(@Param("list")List<WebUserC> listp);

    List<String> getAllMobile();

    List<WebUserC> findByMobile(@Param("mobile") String mobile);

    void update(@Param("webUserC") WebUserC webUserC);

    void del(@Param("list")List<WebUserC> del);

    Map<String, Object> findTotal(@Param("list")List<String> mobiles);
}
