package org.spring.springboot.response;

import org.assertj.core.util.Maps;
import org.springframework.beans.BeanUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 90
 */
public class Result {

    private int result = 1;
    private String msg = "成功";
    private Map<String, Object> data = new HashMap<String, Object>();

    public int getResult() {
        return result;
    }

    public Result fail() {
        this.result = -1;
        return this;
    }

    public Result forbidden() {
        this.result = -2;
        return this;
    }

    public Result ok() {
        this.result = 1;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public Result setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Object getData() {
        return data;
    }

    public Result setData(Map<String,Object> data){

        this.data = data;

        /*Map<String, String> map = BeanUtils.describe(data);
        map.remove("class");
        map.remove("empty");
        Map<String, String> result = Maps.newHashMap();
        for (Map.Entry item : map.entrySet()) {
            String value = (String) item.getValue();

            if (null == item.getValue()) {
                value = "";
            }
            result.put((String) item.getKey(), value);
        }
        this.data = result;*/
        return this;
    }

    /*public Result inject(Object data) {
        this.data = data;
        return this;
    }*/
}
