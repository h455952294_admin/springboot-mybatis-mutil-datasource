package org.spring.springboot.service;

import org.spring.springboot.domain.MemberProulette;

import java.util.List;

/**
 * 用户业务接口层
 *
 * Created by bysocket on 07/02/2017.
 */
public interface MemberProuletteService {

    void batchSave(List<MemberProulette> list);

}
