package org.spring.springboot.service;

import org.spring.springboot.domain.WebUser;

import java.util.List;
import java.util.Map;

/**
 * 用户业务接口层
 *
 * Created by bysocket on 07/02/2017.
 */
public interface WebUserMService {

    List<WebUser> findAll();

    void addDate(WebUser webUser);

    List<WebUser> findByDate(Map<String, Object> param);

    List<String> getAllMobile();

}
