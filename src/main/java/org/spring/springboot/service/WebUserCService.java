package org.spring.springboot.service;

import org.spring.springboot.domain.WebUserC;

import java.util.List;
import java.util.Map;

/**
 * 用户业务接口层
 *
 * Created by bysocket on 07/02/2017.
 */
public interface WebUserCService {

    //List<WebUser> findAll();

    //void addDate(WebUser webUser);


    void insertDate(WebUserC webUserC);
    void updateDate(WebUserC webUserC);
    WebUserC findByMId(String phone);
    void insertMemberAuthority(Map<String, Object> map);

    List<Map<String,Object>> findMemberAuthority(String phone);

    List<WebUserC> batchSave(List<WebUserC> listp);

    List<String> getAllMobile();

    List<WebUserC> findByMobile(String mobile);

    void update(WebUserC webUserC);

    void del(List<WebUserC> del);

    Map<String, Object> findTotal(List<String> mobiles);
}
