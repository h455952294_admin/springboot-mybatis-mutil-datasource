package org.spring.springboot.service.impl;

import org.spring.springboot.dao.master.WebUserMDao;
import org.spring.springboot.domain.WebUser;
import org.spring.springboot.service.WebUserMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 用户业务实现层
 *
 * Created by bysocket on 07/02/2017.
 */
@Service
public class WebUserMServiceImpl implements WebUserMService {

    @Autowired
    private WebUserMDao webUserMDao; // 主数据源

    @Override
    public List<WebUser> findAll() {
        return webUserMDao.findAll();
    }

    @Override
    public void addDate(WebUser webUser) {
        webUserMDao.addDate(webUser);
    }

    @Override
    public List<WebUser> findByDate(Map<String, Object> param) {
        return webUserMDao.findByDate(param);
    }

    @Override
    public List<String> getAllMobile() {
        return webUserMDao.getAllMobile();
    }
}
