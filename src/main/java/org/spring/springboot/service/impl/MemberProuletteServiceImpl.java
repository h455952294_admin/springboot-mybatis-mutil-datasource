package org.spring.springboot.service.impl;

import org.spring.springboot.dao.cluster.MemberProuletteCDao;
import org.spring.springboot.domain.MemberProulette;
import org.spring.springboot.service.MemberProuletteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberProuletteServiceImpl implements MemberProuletteService {

    @Autowired
    private MemberProuletteCDao memberProuletteCDao;

    @Override
    public void batchSave(List<MemberProulette> list) {
        memberProuletteCDao.batchSave(list);
    }
}
