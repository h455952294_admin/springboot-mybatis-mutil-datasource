package org.spring.springboot.service.impl;

import org.spring.springboot.dao.cluster.WebUserCDao;
import org.spring.springboot.domain.WebUser;
import org.spring.springboot.domain.WebUserC;
import org.spring.springboot.service.WebUserCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 用户业务实现层
 *
 * Created by bysocket on 07/02/2017.
 */
@Service
public class WebUserCServiceImpl implements WebUserCService {

    @Autowired
    private WebUserCDao webUserCDao; // 主数据源

    /*@Override
    public List<WebUser> findAll() {
        return webUserCDao.findAll();
    }

    @Override
    public void addDate(WebUser webUser) {
        webUserCDao.addDate(webUser);
    }*/

    @Override
    public void insertDate(WebUserC webUserC) {
        webUserCDao.insertDate(webUserC);
    }

    @Override
    public void updateDate(WebUserC webUserC) {
        webUserCDao.updateDate(webUserC);
    }

    @Override
    public WebUserC findByMId(String phone) {
       return webUserCDao.findByMId(phone);
    }

    @Override
    public void insertMemberAuthority(Map<String, Object> map) {
        webUserCDao.insertMemberAuthority(map);
    }

    @Override
    public List<Map<String, Object>> findMemberAuthority(String phone) {
        return webUserCDao.findMemberAuthority(phone);
    }

    @Override
    public List<WebUserC> batchSave(List<WebUserC> listp) {
        webUserCDao.batchSave(listp);
        return listp;
    }

    @Override
    public List<String> getAllMobile() {
        return webUserCDao.getAllMobile();
    }

    @Override
    public List<WebUserC> findByMobile(String mobile) {
        return webUserCDao.findByMobile(mobile);
    }

    @Override
    public void update(WebUserC webUserC) {
        webUserCDao.update(webUserC);
    }

    @Override
    public void del(List<WebUserC> del) {
        webUserCDao.del(del);
    }

    @Override
    public Map<String, Object> findTotal(List<String> mobiles) {
        return webUserCDao.findTotal(mobiles);
    }
}
