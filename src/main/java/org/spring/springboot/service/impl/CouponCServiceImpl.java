package org.spring.springboot.service.impl;

import org.spring.springboot.dao.cluster.CouponCDao;
import org.spring.springboot.domain.CouponC;
import org.spring.springboot.service.CouponCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CouponCServiceImpl implements CouponCService {

    @Autowired
    private CouponCDao couponDao;

    @Override
    public CouponC readCoupon(String couponId) {
        return couponDao.readCoupon(couponId);
    }
}
