package org.spring.springboot.service.impl;

import org.spring.springboot.dao.cluster.CouponMemberCDao;
import org.spring.springboot.domain.CouponMemberC;
import org.spring.springboot.service.CouponMemberCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CouponMemberCServiceImpl implements CouponMemberCService {

    @Autowired
    private CouponMemberCDao couponMemberCDao;

    @Override
    public void batchSave(List<CouponMemberC> couponMemberCs) {
        couponMemberCDao.batchSave(couponMemberCs);
    }
}
