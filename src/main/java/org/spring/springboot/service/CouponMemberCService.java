package org.spring.springboot.service;

import org.spring.springboot.domain.CouponMemberC;

import java.util.List;

/**
 * 用户业务接口层
 *
 * Created by bysocket on 07/02/2017.
 */
public interface CouponMemberCService {

    void batchSave(List<CouponMemberC> couponMemberCs);
}
