package org.spring.springboot.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyThread  implements Runnable{

    @Autowired
    private TaskProDateSync taskProDateSync;
    @Override
    public void run() {
        System.out.println("MyThread - begin");
        long start = System.currentTimeMillis();
        for (Integer i = 0; i < 610; i++) {
            try {
                taskProDateSync.doTaskSysn(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("MyThread - end 耗时：" + (end - start) + "毫秒");
    }
}
