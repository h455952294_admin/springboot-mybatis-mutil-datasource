package org.spring.springboot.task;

import org.spring.springboot.domain.*;
import org.spring.springboot.service.*;
import org.spring.springboot.util.DateUtil;
import org.spring.springboot.util.SyncLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

@Component
public class TaskProDateSync {
    @Autowired
    private WebUserMService webUserMService;
    @Autowired
    protected WebUserCService webUserCService;
    @Autowired
    protected CouponCService couponCService;
    @Autowired
    protected CouponMemberCService couponMemberCService;
    @Autowired
    protected MemberProuletteService memberProuletteService;

    private long count = 0;

    @Async("taskExecutor")
    public void doTaskSysn(Integer threadNum) throws Exception {
        System.out.println("doTaskSysn - begin 线程 [" + threadNum + "]");
        long start = System.currentTimeMillis();
        doTaskSysnDate(threadNum);
        long end = System.currentTimeMillis();
        System.out.println("doTaskSysn - end 线程 [" + threadNum + "] 耗时：" + (end - start) + "毫秒");
    }

    /**
     * 时间区间[2017-01-01,2018-09-01]
     * 开启800个线程,一个线程2天
     *
     * @param threadNum 线程编号,1开始
     **/
    private void doTaskSysnDate(Integer threadNum) {
        try {
//            String beginDate = DateUtil.addMonth(("2017-01-01"), threadNum * 2);
//            String endDate = DateUtil.addMonth("2017-01-01", (threadNum+1) * 2);
            String beginDate = DateUtil.addDay(("2017-01-01"), threadNum);
            String endDate = DateUtil.addDay("2017-01-01", (threadNum + 1));
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("beginDate", beginDate);
            param.put("endDate", endDate);
            //查询时间区间数据
            List<WebUser> list = webUserMService.findByDate(param);
            System.out.println("----时间[" + DateUtil.DateToString(new Date(), "yyyy-MM-dd mm:HH:ss") + "] 线程[" + threadNum + "] 查询数据为 : [" + list.size() + "]条 " +
                    "----" + "开始日期:" + beginDate + ",结束日期:" + endDate);

            //CouponC couponC = couponCService.readCoupon("1");
            if (list != null && list.size() > 0) {
                List<WebUserC> listc = new ArrayList<WebUserC>();
                for (WebUser webUser : list) {
                    //插数据
                    //webUserCService.addDate(webUser);
                    WebUserC webUserC = new WebUserC();
                    webUserC.setJoinDate(DateUtil.getDate(webUser.getCreateTime()));
                    //webUserC.setCreateTime(System);
                    webUserC.setMobile(webUser.getPhone());
                    webUserC.setImgUrl(webUser.getHeaderImg());
                    String mName = "";
                    if(!StringUtils.isEmpty(webUser.getNickname())){
                        if (webUser.getNickname().length() > 16) {
                            mName = webUser.getNickname().substring(0, 16);
                        }
                    }
                    webUserC.setmName(mName);
                    webUserC.setGender(webUser.getSex() + "");
                    webUserC.setWbOpentid(webUser.getWeibo());
                    webUserC.setQqOpentid(webUser.getQq());
                    webUserC.setUnionid(webUser.getWechat());
                    webUserC.setWapOpentid(webUser.getOpenid());
                    webUserC.setEmail(webUser.getEmail());
                    webUserC.setWhiteRice(0);
                    Integer gold = 0;
                    if(webUser.getGoldRice() != null){
                        gold = webUser.getGoldRice();
                    }
                    webUserC.setGoldRice(gold);
                    Integer color = 0;
                    if(webUser.getColorRice() != null){
                        color = webUser.getColorRice();
                    }
                    webUserC.setColorRice(color);
                    webUserC.setColoring(0);
                    webUserC.setBalance(0d);
                    webUserC.setRoulette(0);
                    webUserC.setPremiumRoulette(0); //赠送一次
                    webUserC.setmPw(webUser.getPassword());
                    webUserC.setLevel(1);
                    webUserC.setIsOld("1");
                    //listc.add(webUserC);
                    //判断是否存在,是:更新,否插数据
                    if (!StringUtils.isEmpty(webUser.getPhone())) {
                        //WebUserC dbWebUser = webUserCService.findByMId(webUser.getPhone());
                        //webUserCService.insertDate(webUserC);
                        listc.add(webUserC);

                        //权限相关
                        /*Map<String, Object> map = new HashMap<String, Object>();
                        map.put("a",webUserC.getmId());
                        map.put("b","role_member");
                        map.put("c","2");
                        webUserCService.insertMemberAuthority(map);*/

                        /*if(null != dbWebUser){
                            webUserCService.updateDate(webUserC);
                        }else{
                            //webUserC.setmPw("7c4a8d09ca3762af61e59520943dc26494f8941b");//123456
                        }
                        //查询是否有加入权限
                        List<Map<String,Object>> auList = webUserCService.findMemberAuthority(webUser.getPhone());
                        if (auList == null || auList.size() == 0) {

                        }*/
                    }
                }
                int i = 0;
                if (listc != null && listc.size() > 0) {
                    Integer pageno = 1;
                    Integer pagesize = 1000;
                    Integer total = listc.size();
                    Integer size = (listc.size() / 1000) + (listc.size() % 1000 == 0 ? 0 : 1);
                    if (total >= 1000) {
                        for (int p = 1; p <= size; p++) {
                            System.out.println("p:" + p + "pagesize:" + pagesize + "size:" + size + ",listc:" + listc.size());
                            List<WebUserC> listp = listc.subList((p - 1) * pagesize, p * pagesize > total ? (total) : p * pagesize);
                            listp = webUserCService.batchSave(listp);
                            //batchSaveCouponMember(couponC, listp);//送优惠券
                            //System.out.println("----时间[" + DateUtil.DateToString(new Date(), "yyyy-MM-dd mm:HH:ss") + "] 线程[" + threadNum + "]目前操作了:[" + (i + listp.size()) + "]条 ----");
                        }
                    } else if (total > 0 && total < 1000) {
                        List<WebUserC> listp = webUserCService.batchSave(listc);
                        //batchSaveCouponMember(couponC, listp);//送优惠券
                        //System.out.println("----时间[" + DateUtil.DateToString(new Date(), "yyyy-MM-dd mm:HH:ss") + "]  线程[" + threadNum + "]目前操作了:[" + listp.size() + "]条 ---- 结束");
                    }

                    //每1000条打印一下
                    /*if (i % 1000 == 0) {
                        System.out.println("----时间[" + DateUtil.DateToString(new Date(), "yyyy-MM-dd mm:HH:ss") + "] 线程[" + threadNum + "]目前操作了:[" + i + "]条 ----");
                    }
                    if (i == list.size()) {
                        System.out.println("----时间[" + DateUtil.DateToString(new Date(), "yyyy-MM-dd mm:HH:ss") + "]  线程[" + threadNum + "]目前操作了:[" + i + "]条 ---- 结束");
                    }
                    i++;*/
                }
                synchronized (SyncLock.getLock(count + "")) {
                    count += list.size();
                    System.out.println("----时间[" + DateUtil.DateToString(new Date(), "yyyy-MM-dd mm:HH:ss") + "]  线程[" + threadNum + "]目前总操作了:[" + count + "]条 ---- 结束");
                }

            }
        } catch (Exception e) {
            System.out.println("错误线程[" + threadNum + "]");
            e.printStackTrace();
        }
    }

    //批量保存相关信息
    private void batchSaveCouponMember(CouponC couponC, List<WebUserC> listp) {
        List<CouponMemberC> memberCS = new ArrayList<CouponMemberC>();
        List<MemberProulette> memberProulettes = new ArrayList<MemberProulette>();
        for (WebUserC webUserC : listp) {//保存优惠券,和优惠券记录
            /*CouponMemberC couponMemberC = new CouponMemberC();
            couponMemberC.setmId(webUserC.getmId());
            couponMemberC.setActBeginTime(couponC.getActBeginTime());
            couponMemberC.setActEndTime(couponC.getActEndTime());
            couponMemberC.setCouponId(couponC.getCouponId() + "");
            couponMemberC.setDisType(couponC.getDisType());
            couponMemberC.setIsUse("0");
            if ("1".equals(couponC.getDisType())) {//固定金额
                couponMemberC.setDisAmount(couponC.getDisAmount());
            } else {//随机金额
                couponMemberC.setDisAmount(0d);
                couponMemberC.setRandomAmountBegin(couponC.getRandomAmountBegin());
                couponMemberC.setRandomAmountEnd(couponC.getRandomAmountEnd());
                couponMemberC.setRandomAmount(getRandomAmount(couponC.getRandomAmountBegin(), couponC.getRandomAmountEnd()));
            }
            couponMemberC.setReceiveType(couponC.getReceiveType());
            couponMemberC.setRuleAmount(couponC.getRuleAmount());
            couponMemberC.setTitle(couponC.getTitle());
            couponMemberC.setUseBeginTime(couponC.getUseBeginTime());
            couponMemberC.setUseEndTime(couponC.getUseEndTime());
            couponMemberC.setType(couponC.getType());
            couponMemberC.setUseProduct(couponMemberC.getUseProduct());
            couponMemberC.setUseType(couponC.getUseType());
            memberCS.add(couponMemberC);*/

            //优惠券记录
            MemberProulette memberProulette = new MemberProulette();
            memberProulette.setIsGJ("1");
            memberProulette.setIsKp("1");
            memberProulette.setmId(webUserC.getmId());
            memberProulettes.add(memberProulette);
        }
        /*if (memberCS != null && memberCS.size() > 0) {//批量保存
            couponMemberCService.batchSave(memberCS);
        }*/
        if (memberProulettes != null && memberProulettes.size() > 0) {
            memberProuletteService.batchSave(memberProulettes);
        }
    }

    public Double getRandomAmount(Double begin, Double end) {
        //要获取一个[x,y]的double类型的随机数 | 左闭右闭
        double d = begin + Math.random() * end % (end - begin + 1);
        return  BigDecimal.valueOf(d).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();//两位小数
    }

    @Async("taskExecutor")
    public void doTaskDelAndUpdate(List<String> mobiles) {
        for(String mobile :mobiles){
            List<WebUserC> webUserCS = webUserCService.findByMobile(mobile);
            if(webUserCS !=null && webUserCS.size()>0){
                WebUserC webUserC = webUserCS.get(0);
                List<WebUserC> del = webUserCS.subList(1, webUserCS.size());
                for(WebUserC webu : del){
                    webUserC.setGoldRice(webUserC.getGoldRice() + webu.getGoldRice());
                    webUserC.setColorRice(webUserC.getColorRice() + webu.getColorRice());
                }
                webUserCService.update(webUserC);
                webUserCService.del(del);
            }
        }
    }
}
