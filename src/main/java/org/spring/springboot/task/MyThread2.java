package org.spring.springboot.task;

import org.spring.springboot.service.WebUserCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MyThread2 implements Runnable{

    @Autowired
    private WebUserCService webUserCService;

    @Autowired
    private TaskProDateSync taskProDateSync;

    @Override
    public void run() {
        System.out.println("MyThread2 - begin");
        long start = System.currentTimeMillis();
        //获取所有重复手机号
        List<String> mobiles = webUserCService.getAllMobile();
        if (mobiles != null && mobiles.size() > 0) {
            Integer pageno = 1;
            Integer pagesize = 100;
            Integer total = mobiles.size();
            Integer size = mobiles.size() / 100 + (mobiles.size() % 100 == 0 ? 0 : 1);
            for (int p = 1; p <= size; p++) {
                //System.out.println("p:" + p + "pagesize:" + pagesize + "size:" + size + ",listc:" + listc.size());
                List<String> listp = mobiles.subList((p - 1) * pagesize, p * pagesize > total ? (total) : p * pagesize);
                taskProDateSync.doTaskDelAndUpdate(listp);
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("MyThread2 - end 耗时：" + (end - start) + "毫秒");
    }
}
