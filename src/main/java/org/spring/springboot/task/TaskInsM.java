package org.spring.springboot.task;

import org.spring.springboot.domain.WebUser;
import org.spring.springboot.service.WebUserMService;
import org.spring.springboot.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

@Component
public class TaskInsM {
    @Autowired
    private WebUserMService webUserMService;

    @Async("taskExecutor")
    public void doTaskOne() throws Exception {
        System.out.println("一组开始");
        long start = System.currentTimeMillis();
        insertDate(10000);
        long end = System.currentTimeMillis();
        System.out.println("完成任务一，耗时：" + (end - start) + "毫秒");
    }


    @Async("taskExecutor")
    public void doTaskTwo() throws Exception {
        System.out.println("二组开始");
        long start = System.currentTimeMillis();
        insertDate(10000);
        long end = System.currentTimeMillis();
        System.out.println("完成任务二，耗时：" + (end - start) + "毫秒");
    }

    @Async("taskExecutor")
    public void doTaskThree() throws Exception {
        System.out.println("三组开始");
        long start = System.currentTimeMillis();
        insertDate(10000);
        long end = System.currentTimeMillis();
        System.out.println("完成任务三，耗时：" + (end - start) + "毫秒");
    }

    @Async("taskExecutor")
    public void doTaskFour(Integer num,Integer num2) throws Exception {
        System.out.println("----时间["+ DateUtil.DateToString(new Date(),"yyyy-MM-dd mm:HH:ss") +"四组开始,线程[" + num2+"]");
        long start = System.currentTimeMillis();
        insertDateThread(num, num2);
        long end = System.currentTimeMillis();
        System.out.println("----时间["+ DateUtil.DateToString(new Date(),"yyyy-MM-dd mm:HH:ss") +"完成任务四,线程["+num2+"],耗时：" + (end - start) + "毫秒");

    }

    private void insertDateThread(Integer num, Integer num2) {
        for(int i=1 ;i<=num;i++){
            String uuid = UUID.randomUUID() + "";
            WebUser webUser = new WebUser();
            webUser.setId(uuid);
            webUser.setUsername("test" + uuid);
            webUser.setPassword("123" + uuid);
            webUser.setNickname("test" + uuid);
            webUser.setCreateTime(randomDate());
            webUserMService.addDate(webUser);
            //每1000条打印一下
            if(i%1000 == 0){
                System.out.println("----时间[" + DateUtil.DateToString(new Date(), "yyyy-MM-dd mm:HH:ss") + "] 线程[" + num2 + "]目前插入了:[" + i + "]条 ----");
            }if(i == num){
                System.out.println("----时间["+ DateUtil.DateToString(new Date(), "yyyy-MM-dd mm:HH:ss") +"]  线程["+num2+"]目前插入了:[" + i + "]条 ----");
            }
        }
    }

    public Date randomDate(){
        Random random2 = new Random();
        int r2 = random2.nextInt(11) % 10 + 1;

        Date date;
        date = DateUtil.addMonth(DateUtil.StringToDate("2018-01-01"), r2);

        return date;
    }

    private void insertDate(Integer num) {
        for(int i=0 ;i<num;i++){
            String uuid = UUID.randomUUID() + "";
            WebUser webUser = new WebUser();
            webUser.setId(uuid);
            webUser.setUsername("test" + uuid);
            webUser.setPassword("123" + uuid);
            webUser.setNickname("test" + uuid);
            webUserMService.addDate(webUser);
            //每1000条打印一下
            if(i%1000 == 0){
                System.out.println("目前插入了:" + num + "条");
            }
        }
    }

}
