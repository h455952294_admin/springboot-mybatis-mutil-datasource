package org.spring.springboot.task;

import org.spring.springboot.service.WebUserCService;
import org.spring.springboot.service.WebUserMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class MyThread3 implements Runnable{

    @Autowired
    private WebUserCService webUserCService;
    @Autowired
    private WebUserMService webUserMService;

    @Autowired
    private TaskProDateSync taskProDateSync;

    @Override
    public void run() {
        System.out.println("MyThread3 - begin");
        long start = System.currentTimeMillis();
        //获取所有重复手机号
        List<String> mobiles = webUserMService.getAllMobile();

        Map<String, Object> result = webUserCService.findTotal(mobiles);
        if(result!=null){
            System.out.println("goldRice:" + result.get("goldRice"));
            System.out.println("colorRice:" + result.get("colorRice"));
        }

        long end = System.currentTimeMillis();
        System.out.println("MyThread3 - end 耗时：" + (end - start) + "毫秒");
    }
}
