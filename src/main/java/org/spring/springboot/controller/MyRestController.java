package org.spring.springboot.controller;

import org.spring.springboot.domain.WebUser;
import org.spring.springboot.response.Result;
import org.spring.springboot.service.BusinessService;
import org.spring.springboot.service.WebUserCService;
import org.spring.springboot.service.WebUserMService;
import org.spring.springboot.task.*;
import org.spring.springboot.util.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户控制层
 *
 * Created by bysocket on 07/02/2017.
 */
@RestController
public class MyRestController {

    @Autowired
    private WebUserMService webUserMService;

    @Autowired
    private WebUserCService webUserCService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private Task task;

    @Autowired
    private TaskInsM taskInsM;
    @Autowired
    private TaskProDateSync taskProDateSync;



    private static Boolean isOpen = false;
    private static Boolean isOpenPro = false;

    /**
     *
     * @return
     */
    @RequestMapping(value = "/api/all", method = RequestMethod.GET)
    public Object all() {
        List<WebUser> listm = webUserMService.findAll();
        //List<WebUser> listc = webUserCService.findAll();
        return new Result().ok();
    }

    /**
     * 本地测试添加
     * **/
    @RequestMapping(value = "/api/add", method = RequestMethod.GET)
    public Object add() throws Exception {
        Result result = new Result();
        if(isOpen){
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("msg","正在添加");
            map.put("isOpen", isOpen);
            result.setData(map);
            return result.ok();
        }
        isOpen = true;
        //开启10条线程
        for (Integer i = 0; i < 10; i++) {
            taskInsM.doTaskFour(10, i);
        }
        Thread.currentThread().join();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg","添加成功");
        map.put("isOpen", isOpen);
        result.setData(map);
        return result.ok();
    }

    /**
     * 多线程生产数据同步
     * */
    @RequestMapping(value = "/api/addByProduct", method = RequestMethod.GET)
    public Object addByProduct() throws Exception {
        Result result = new Result();
        if(isOpenPro){
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("msg","正在添加");
            map.put("isOpenPro", isOpenPro);
            result.setData(map);
            return result.ok();
        }
        isOpenPro = true;
        //开启线程
        /*for (Integer i = 0; i < 800; i++) {
            taskProDateSync.doTaskSysn(i);
        }
        Thread.currentThread().join();*/
        MyThread myThread = (MyThread)SpringUtil.getBean("myThread");
        new Thread(myThread).start();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg","添加成功");
        map.put("isOpen", isOpenPro);
        result.setData(map);
        return result.ok();
    }

    @RequestMapping(value = "/api/addDel", method = RequestMethod.GET)
    public Object addDel() throws Exception {
        Result result = new Result();
        if(isOpenPro){
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("msg","正在添加");
            map.put("isOpenPro", isOpenPro);
            result.setData(map);
            return result.ok();
        }
        isOpenPro = true;
        //开启线程
        /*for (Integer i = 0; i < 800; i++) {
            taskProDateSync.doTaskSysn(i);
        }
        Thread.currentThread().join();*/
        MyThread2 myThread2 = (MyThread2)SpringUtil.getBean("myThread2");
        new Thread(myThread2).start();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg","添加成功");
        map.put("isOpen", isOpenPro);
        result.setData(map);
        return result.ok();
    }

    @RequestMapping(value = "/api/total", method = RequestMethod.GET)
    public Object total() throws Exception {
        Result result = new Result();
        //开启线程
        /*for (Integer i = 0; i < 800; i++) {
            taskProDateSync.doTaskSysn(i);
        }
        Thread.currentThread().join();*/
        MyThread3 myThread3 = (MyThread3)SpringUtil.getBean("myThread3");
        new Thread(myThread3).start();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg","添加成功");
        map.put("isOpen", isOpenPro);
        result.setData(map);
        return result.ok();
    }

    /**
     * 更新是否开启为false
     * */
    @RequestMapping(value = "/api/updateIsOPen", method = RequestMethod.GET)
    public Object updateIsOPen(){
        Result result = new Result();
        if (isOpen) {
            isOpen = false;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("isOpen", isOpen);
        result.setData(map);
        return result.ok();
    }

    /**
     * 更新是否开启为false
     * */
    @RequestMapping(value = "/api/updateIsOPenPro", method = RequestMethod.GET)
    public Object updateIsOPenPro(){
        Result result = new Result();
        if (isOpenPro) {
            isOpenPro = false;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("isOpen", isOpenPro);
        result.setData(map);
        return result.ok();
    }

    /**
     * 根据id更新数据
     * */
    @RequestMapping(value = "/api/update", method = RequestMethod.GET)
    public Object update(@RequestParam(value = "id", required = true) String id){
        WebUser webUser = new WebUser();
        webUser.setId(id);
        webUser.setUsername("testUpdate");
        webUser.setPassword("123Update");
        webUser.setNickname("testUpdate");
        //webUserCService.updateDate(webUser);
        Result result = new Result();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg","更新成功");
        result.setData(map);
        return result.ok();
    }

    /**
     *
     * */
    @RequestMapping(value = "/api/sync", method = RequestMethod.GET)
    public Object syncDate() throws Exception {
        Result result = new Result();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg","同步成功");
        result.setData(map);
        return result.ok();
    }

}
