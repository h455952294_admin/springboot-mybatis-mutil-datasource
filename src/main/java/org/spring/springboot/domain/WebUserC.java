package org.spring.springboot.domain;



public class WebUserC {

    private Long mId;
    private String mPw;
    private String mName;
    private String email;
    private String gender;
    private String birth;
    private String tel;
    private String type;
    private String joinDate;
    private String pushToken;
    private String snsToken;
    private String snsType;
    private String pushYn;
    private String emailYn;
    private String smsYn;
    private String imgUrl;
    private String group_idx;
    private String artist_idx;
    //private int pinkHeart;
    //private int redHeart;
    private int whiteRice;
    private int goldRice;
    private int level;
    private int roulette;
    private int premiumRoulette;
    private String school;
    private String dropYn;
    private String updateSchoolDate;
    private String phoneYn;
    private int coloring;
    private String os;

    //手机号码
    private String mobile;
    //微信wap端 openid
    private String wapOpentid;
    //微信小程序 openid
    private String srOpentid;
    //微信unionid
    private String unionid;
    //微博openid
    private String wbOpentid;
    //qqopenid
    private String qqOpentid;
    //最新登录时间
    private Long lastLoginTime;
    //创建时间-秒
    private Long createTime;
    private String source;//是哪一端,1:android,2:ios,3:wap
    private String equipMark;//设备标识
    private int colorRice; //七彩饭团
    private Double balance; //账户余额
    private String  isResetPwd; //是否重置密码 1:是,0:否
    private String  isLock; //是否冻结,1:是,0:否
    private String  leverName; //等级名称
    private String  isOld; //是否老用户,1:是,0:否

    public String getIsOld() {
        return isOld;
    }

    public void setIsOld(String isOld) {
        this.isOld = isOld;
    }

    public Long getmId() {
        return mId;
    }

    public void setmId(Long mId) {
        this.mId = mId;
    }

    public String getPushYn() {
        return pushYn;
    }

    public void setPushYn(String pushYn) {
        this.pushYn = pushYn;
    }

    public String getEmailYn() {
        return emailYn;
    }

    public void setEmailYn(String emailYn) {
        this.emailYn = emailYn;
    }

    public String getSmsYn() {
        return smsYn;
    }

    public void setSmsYn(String smsYn) {
        this.smsYn = smsYn;
    }

    public String getArtist_idx() {
        return artist_idx;
    }

    public void setArtist_idx(String artist_idx) {
        this.artist_idx = artist_idx;
    }

    public int getWhiteRice() {
        return whiteRice;
    }

    public void setWhiteRice(int whiteRice) {
        this.whiteRice = whiteRice;
    }

    public int getGoldRice() {
        return goldRice;
    }

    public void setGoldRice(int goldRice) {
        this.goldRice = goldRice;
    }

    public Long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getEquipMark() {
        return equipMark;
    }

    public void setEquipMark(String equipMark) {
        this.equipMark = equipMark;
    }

    public int getColorRice() {
        return colorRice;
    }

    public void setColorRice(int colorRice) {
        this.colorRice = colorRice;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getIsResetPwd() {
        return isResetPwd;
    }

    public void setIsResetPwd(String isResetPwd) {
        this.isResetPwd = isResetPwd;
    }

    public String getIsLock() {
        return isLock;
    }

    public void setIsLock(String isLock) {
        this.isLock = isLock;
    }

    public String getLeverName() {
        return leverName;
    }

    public void setLeverName(String leverName) {
        this.leverName = leverName;
    }

    public String getmPw() {
        return mPw;
    }

    public void setmPw(String mPw) {
        this.mPw = mPw;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getSnsToken() {
        return snsToken;
    }

    public void setSnsToken(String snsToken) {
        this.snsToken = snsToken;
    }

    public String getSnsType() {
        return snsType;
    }

    public void setSnsType(String snsType) {
        this.snsType = snsType;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getGroup_idx() {
        return group_idx;
    }

    public void setGroup_idx(String group_idx) {
        this.group_idx = group_idx;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getRoulette() {
        return roulette;
    }

    public void setRoulette(int roulette) {
        this.roulette = roulette;
    }

    public int getPremiumRoulette() {
        return premiumRoulette;
    }

    public void setPremiumRoulette(int premiumRoulette) {
        this.premiumRoulette = premiumRoulette;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getDropYn() {
        return dropYn;
    }

    public void setDropYn(String dropYn) {
        this.dropYn = dropYn;
    }

    public String getUpdateSchoolDate() {
        return updateSchoolDate;
    }

    public void setUpdateSchoolDate(String updateSchoolDate) {
        this.updateSchoolDate = updateSchoolDate;
    }

    public String getPhoneYn() {
        return phoneYn;
    }

    public void setPhoneYn(String phoneYn) {
        this.phoneYn = phoneYn;
    }

    public int getColoring() {
        return coloring;
    }

    public void setColoring(int coloring) {
        this.coloring = coloring;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWapOpentid() {
        return wapOpentid;
    }

    public void setWapOpentid(String wapOpentid) {
        this.wapOpentid = wapOpentid;
    }

    public String getSrOpentid() {
        return srOpentid;
    }

    public void setSrOpentid(String srOpentid) {
        this.srOpentid = srOpentid;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getWbOpentid() {
        return wbOpentid;
    }

    public void setWbOpentid(String wbOpentid) {
        this.wbOpentid = wbOpentid;
    }

    public String getQqOpentid() {
        return qqOpentid;
    }

    public void setQqOpentid(String qqOpentid) {
        this.qqOpentid = qqOpentid;
    }
}
