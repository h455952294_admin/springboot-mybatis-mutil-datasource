package org.spring.springboot.domain;
/**
 *
 * 优惠卷信息表
 * */
public class CouponC {

    private Integer couponId;
    private Integer total;//优惠券发放总数
    private Integer useTotal;//优惠券使用总数
    private Integer perQuantity;//每人可领取数量
    private Double ruleAmount;//规定使用金额
    private Double disAmount;//优惠金额
    private Long actBeginTime;//活动开始时间
    private Long actEndTime;//活动结束时间
    private Long useBeginTime;//有效开始时间
    private Long useEndTime;//有效结束时间
    private Long createTime;//创建时间
    private Long updateTime;//更新时间
    private Long deleteTime;//删除时间
    private String isSale;//是否上架,1:上架,0:不上架
    private String isShow;//是否展示,1:展示,0:不展示
    private String useType;//使用场景,0:全平台,1:钱包,2:饭团
    private String useProduct;//优惠商品id
    private String disType;//优惠类型,1:固定金额,2:随机金额
    private Double randomAmountBegin;//随机金额开始
    private Double randomAmountEnd;//随机金额结束
    private String type;//类型,1.普通优惠券,2:新注册用户优惠券
    private String receiveType;//优惠类型,领取者身份,1:等级1,2:等级2
    private String title;

    private String actBeginTimeStr;//活动开始时间
    private String actEndTimeStr;//活动结束时间
    private String useBeginTimeStr;//有效开始时间
    private String useEndTimeStr;//有效结束时间
    private String createTimeStr;//创建时间
    private String updateTimeStr;//更新时间
    private String deleteTimeStr;//删除时间

    public String getActBeginTimeStr() {
        return actBeginTimeStr;
    }

    public void setActBeginTimeStr(String actBeginTimeStr) {
        this.actBeginTimeStr = actBeginTimeStr;
    }

    public String getActEndTimeStr() {
        return actEndTimeStr;
    }

    public void setActEndTimeStr(String actEndTimeStr) {
        this.actEndTimeStr = actEndTimeStr;
    }

    public String getUseBeginTimeStr() {
        return useBeginTimeStr;
    }

    public void setUseBeginTimeStr(String useBeginTimeStr) {
        this.useBeginTimeStr = useBeginTimeStr;
    }

    public String getUseEndTimeStr() {
        return useEndTimeStr;
    }

    public void setUseEndTimeStr(String useEndTimeStr) {
        this.useEndTimeStr = useEndTimeStr;
    }

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getUpdateTimeStr() {
        return updateTimeStr;
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }

    public String getDeleteTimeStr() {
        return deleteTimeStr;
    }

    public void setDeleteTimeStr(String deleteTimeStr) {
        this.deleteTimeStr = deleteTimeStr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisType() {
        return disType;
    }

    public void setDisType(String disType) {
        this.disType = disType;
    }

    public Double getRandomAmountBegin() {
        return randomAmountBegin;
    }

    public void setRandomAmountBegin(Double randomAmountBegin) {
        this.randomAmountBegin = randomAmountBegin;
    }

    public Double getRandomAmountEnd() {
        return randomAmountEnd;
    }

    public void setRandomAmountEnd(Double randomAmountEnd) {
        this.randomAmountEnd = randomAmountEnd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReceiveType() {
        return receiveType;
    }

    public void setReceiveType(String receiveType) {
        this.receiveType = receiveType;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getUseTotal() {
        return useTotal;
    }

    public void setUseTotal(Integer useTotal) {
        this.useTotal = useTotal;
    }

    public Integer getPerQuantity() {
        return perQuantity;
    }

    public void setPerQuantity(Integer perQuantity) {
        this.perQuantity = perQuantity;
    }

    public Double getRuleAmount() {
        return ruleAmount;
    }

    public void setRuleAmount(Double ruleAmount) {
        this.ruleAmount = ruleAmount;
    }

    public Double getDisAmount() {
        return disAmount;
    }

    public void setDisAmount(Double disAmount) {
        this.disAmount = disAmount;
    }

    public Long getActBeginTime() {
        return actBeginTime;
    }

    public void setActBeginTime(Long actBeginTime) {
        this.actBeginTime = actBeginTime;
    }

    public Long getActEndTime() {
        return actEndTime;
    }

    public void setActEndTime(Long actEndTime) {
        this.actEndTime = actEndTime;
    }

    public Long getUseBeginTime() {
        return useBeginTime;
    }

    public void setUseBeginTime(Long useBeginTime) {
        this.useBeginTime = useBeginTime;
    }

    public Long getUseEndTime() {
        return useEndTime;
    }

    public void setUseEndTime(Long useEndTime) {
        this.useEndTime = useEndTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Long getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Long deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getIsSale() {
        return isSale;
    }

    public void setIsSale(String isSale) {
        this.isSale = isSale;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public String getUseType() {
        return useType;
    }

    public void setUseType(String useType) {
        this.useType = useType;
    }

    public String getUseProduct() {
        return useProduct;
    }

    public void setUseProduct(String useProduct) {
        this.useProduct = useProduct;
    }
}
