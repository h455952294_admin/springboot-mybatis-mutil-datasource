package org.spring.springboot.domain;

public class MemberProulette {

    private Long id ;
    private Long mId ;
    private String isKp ;
    private String isGJ ;
    private Long createTime ;

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getmId() {
        return mId;
    }

    public void setmId(Long mId) {
        this.mId = mId;
    }

    public String getIsKp() {
        return isKp;
    }

    public void setIsKp(String isKp) {
        this.isKp = isKp;
    }

    public String getIsGJ() {
        return isGJ;
    }

    public void setIsGJ(String isGJ) {
        this.isGJ = isGJ;
    }
}
