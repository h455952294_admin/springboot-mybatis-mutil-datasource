package org.spring.springboot.domain;



import java.sql.Timestamp;
import java.util.Date;

public class WebUser {

    private String id;

    private String username;

    private String password;

    /**
     * 关注公众号id
     */
//    @TableField("openid")
    private String openid;

    /**
     * 0 未选择
     * 1 男
     * 2 女
     */
//    @TableField("sex")
    private int sex;

    /**
     * 生日
     */
//    @TableField("birthday")
    private String birthday;

//    @TableField("account_non_locked")
    private boolean accountNonLocked = true;

//    @TableField("access_token")
    private String accessToken;

//    @TableField("create_time")
    private Date createTime;

//    @TableField("last_login_time")
    private Timestamp lastLoginTime;

//    @TableField("header_img")
    private String headerImg;

//    @TableField("nickname")
    private String nickname;

    /**
     * 第三方授权来源
     * QQ
     * SinaWebo
     * WeChat
     * phone
     * email
     */
//    @TableField("authorization_source")
    private String authorizationSource;

    /**
     * 手机
     */
//    @TableField("phone")
    private String phone;


    /**
     * 余额
     */
//    @TableField("balance")
    private double balance;
    /**
     * 个性签名
     */
//    @TableField("signature")
    private String signature;

    /**
     * 用户的邀请码
     */
//    @TableField("invitation_code")
    private String invitationCode;

    /**
     * 用户自身的邀请码
     */
//    @TableField("own_code")
    private String ownCode;

    /**
     * 积分
     */
//    @TableField("integral")
    private int integral;

    /**
     * 白饭团
     */
//    @TableField("white_rice")
    private int whiteRice;

    /**
     * 金饭团
     */
//    @TableField("gold_rice")
    private Integer goldRice;

    /**
     * 七彩饭团
     */
//    @TableField("color_rice")
    private Integer colorRice;

    /**
     * 参与h5活动的时间，用来判断的
     */
//    @TableField("activity_time")
    private Timestamp activityTime;

    //============================//
    /**
     * 邮箱
     */
//    @TableField("email")
    private String email;

    /**
     * 微博唯一识别号
     */
//    @TableField("weibo")
    private String weibo;

    /**
     * QQ唯一识别号
     */
//    @TableField("qq")
    private String qq;

    /**
     * 微信唯一识别号
     */
//    @TableField("wechat")
    private String wechat;

    /**
     * 关注用户数
     */
//    @Transience
    private int attentionNumber;

    /**
     * 粉丝数
     */
//    @Transience
    private int fansNumber;

    /**
     * 等级，根据粉丝数来定
     * 1、新人 1-500
     * 2、一位 501-2500
     * 3、优秀爱豆 2501-10000
     * 4、本赏 10001-100000
     * 5、大赏 100000以上
     */
//    @Transience
    private int lever;

    /**
     * 0、未关注
     * 1、已关注
     */
//    @Transience
    private int isAttention;

    public int getIsAttention() {
        return isAttention;
    }

    public void setIsAttention(int isAttention) {
        this.isAttention = isAttention;
    }

    public int getLever() {
        return lever;
    }

    public void setLever(int lever) {
        this.lever = lever;
    }

    public int getFansNumber() {
        return fansNumber;
    }

    public void setFansNumber(int fansNumber) {
        this.fansNumber = fansNumber;
    }

    public int getAttentionNumber() {
        return attentionNumber;
    }

    public void setAttentionNumber(int attentionNumber) {
        this.attentionNumber = attentionNumber;
    }

    public String getWeibo() {
        return weibo;
    }

    public void setWeibo(String weibo) {
        this.weibo = weibo;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(Timestamp activityTime) {
        this.activityTime = activityTime;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Timestamp getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Timestamp lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getHeaderImg() {
        return headerImg;
    }

    public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAuthorizationSource() {
        return authorizationSource;
    }

    public void setAuthorizationSource(String authorizationSource) {
        this.authorizationSource = authorizationSource;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getOwnCode() {
        return ownCode;
    }

    public void setOwnCode(String ownCode) {
        this.ownCode = ownCode;
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }

    public int getWhiteRice() {
        return whiteRice;
    }

    public void setWhiteRice(int whiteRice) {
        this.whiteRice = whiteRice;
    }

    public Integer getGoldRice() {
        return goldRice;
    }

    public void setGoldRice(Integer goldRice) {
        this.goldRice = goldRice;
    }

    public Integer getColorRice() {
        return colorRice;
    }

    public void setColorRice(Integer colorRice) {
        this.colorRice = colorRice;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }
}
