package org.spring.springboot.domain;
/**
 *
 * 优惠卷信息表
 * */
public class CouponMemberC {

    private Long cmId;
    private Long mId;
    private Double ruleAmount;//规定使用金额
    private Double disAmount;//优惠金额
    private Long actBeginTime;//活动开始时间
    private Long actEndTime;//活动结束时间
    private Long useBeginTime;//有效开始时间
    private Long useEndTime;//有效结束时间
    private Long createTime;//创建时间
    private Long updateTime;//更新时间
    private Long useTime;//使用时间
    private String useType;//使用场景,0:全平台,1:钱包,2:饭团
    private String useProduct;//优惠商品id
    private String isUse;//是否使用,1:已使用,0:未使用
    private String orderId;//订单id
    private String couponId;//优惠券id
    private String disType;//优惠类型,1:固定金额,2:随机金额
    private Double randomAmountBegin;//随机金额开始
    private Double randomAmountEnd;//随机金额结束
    private Double randomAmount;//最终随机金额
    private String type;//类型,1.普通优惠券,2:新注册用户优惠券
    private String receiveType;//优惠类型,领取者身份,1:等级1,2:等级2
    private String title;//标题

    public Double getRandomAmount() {
        return randomAmount;
    }

    public void setRandomAmount(Double randomAmount) {
        this.randomAmount = randomAmount;
    }

    public String getUseType() {
        return useType;
    }

    public void setUseType(String useType) {
        this.useType = useType;
    }

    public String getUseProduct() {
        return useProduct;
    }

    public void setUseProduct(String useProduct) {
        this.useProduct = useProduct;
    }

    public Long getmId() {
        return mId;
    }

    public void setmId(Long mId) {
        this.mId = mId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisType() {
        return disType;
    }

    public void setDisType(String disType) {
        this.disType = disType;
    }

    public Double getRandomAmountBegin() {
        return randomAmountBegin;
    }

    public void setRandomAmountBegin(Double randomAmountBegin) {
        this.randomAmountBegin = randomAmountBegin;
    }

    public Double getRandomAmountEnd() {
        return randomAmountEnd;
    }

    public void setRandomAmountEnd(Double randomAmountEnd) {
        this.randomAmountEnd = randomAmountEnd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReceiveType() {
        return receiveType;
    }

    public void setReceiveType(String receiveType) {
        this.receiveType = receiveType;
    }

    public Long getCmId() {
        return cmId;
    }

    public void setCmId(Long cmId) {
        this.cmId = cmId;
    }

    public Long getUseTime() {
        return useTime;
    }

    public void setUseTime(Long useTime) {
        this.useTime = useTime;
    }

    public String getIsUse() {
        return isUse;
    }

    public void setIsUse(String isUse) {
        this.isUse = isUse;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public Double getRuleAmount() {
        return ruleAmount;
    }

    public void setRuleAmount(Double ruleAmount) {
        this.ruleAmount = ruleAmount;
    }

    public Double getDisAmount() {
        return disAmount;
    }

    public void setDisAmount(Double disAmount) {
        this.disAmount = disAmount;
    }

    public Long getActBeginTime() {
        return actBeginTime;
    }

    public void setActBeginTime(Long actBeginTime) {
        this.actBeginTime = actBeginTime;
    }

    public Long getActEndTime() {
        return actEndTime;
    }

    public void setActEndTime(Long actEndTime) {
        this.actEndTime = actEndTime;
    }

    public Long getUseBeginTime() {
        return useBeginTime;
    }

    public void setUseBeginTime(Long useBeginTime) {
        this.useBeginTime = useBeginTime;
    }

    public Long getUseEndTime() {
        return useEndTime;
    }

    public void setUseEndTime(Long useEndTime) {
        this.useEndTime = useEndTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

}
