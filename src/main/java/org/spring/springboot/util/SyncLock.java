package org.spring.springboot.util;

import com.google.common.collect.Maps;
import java.util.Map;

public class SyncLock {
    private static Map<String, Object> locks = Maps.newConcurrentMap();

    public SyncLock() {
    }

    public static Object getLock(String pid) {
        Object sysnLock = locks.get(pid);
        if (sysnLock == null) {
            Class var2 = SyncLock.class;
            synchronized(SyncLock.class) {
                sysnLock = new Object();
                locks.put(pid, sysnLock);
            }
        }

        return sysnLock;
    }
}
